# C Towers of Hanoi

Towers of Hanoi is a game where the player has to move a stack of blocks in decreasing size
from one place to another. The difficulty being that you can only move one block at a time,
there are three rods where the blocks can be put (including the start positon), and the blocks
must always be stacked in decreasing size, from top to bottom.

## How to run

`towers.exe <no of rods> <stack height>`
