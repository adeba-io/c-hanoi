@echo OFF
Rem Debug script, pass in the name of the file with the main function you wish to
rem run from within the Source folder
gcc -g .\Source\%1.c Source\Towers\*.c -ISource\Towers -o .\Debug\%1.exe
gdb .\Debug\%1.exe
