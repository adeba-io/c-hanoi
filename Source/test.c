#include <stdio.h>

#include "args.h"


int main(int argc, char* argv[])
{
	size_t rods = 0, stackHeight = 0;

	if (parseArgs(argc, argv, &rods, &stackHeight))
		printf("Rods %d Stack %d\n", rods, stackHeight);
	else
		puts("failure");

	return 1;
}
