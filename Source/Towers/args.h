#include <stdio.h>

#define HOW_TO_USE_MSG "Execute this program as such:\n	%s <no of rods> <stack height>"

unsigned char parseArgs(int argc, char *argv[], size_t* rods, size_t* stackHeight);
