#include "board.h"

PlayField newPlayField(size_t rods, size_t stackHeight)
{
	if (stackHeight < MIN_STACK_HEIGHT)
		stackHeight = DEFAULT_STACK_HEIGHT;
	
	if (rods < MIN_NUM_RODS)
		rods = DEFAULT_NUM_RODS;
	
	PlayField field = { NULL, NULL, rods, stackHeight };

	field.tops = (int*) malloc(sizeof(int) * rods);
	field.tops[0] = stackHeight;
	for (int i = 1; i < rods; i++)
	{
		field.tops[i] = 0;
	}

	field.board = malloc(sizeof(int*) * rods);
	for (int i = 0; i < rods; i++)
	{
		field.board[i] = (int*) malloc(sizeof(int) * stackHeight);

		for (int j = 0; j < stackHeight; j++)
		{
			if (i == 0)
			{
				field.board[i][j] = stackHeight - j;
				continue;
			}
 
			field.board[i][j] = 0;
		}
	}

	return field;
}

void printPlayField(const PlayField* field)
{
	printf("PlayField - Rods: %i, Max Stack: %i\n", field->rods, field->stackHeight);

	// Print the rods
	for (int hStack = field->stackHeight - 1; hStack > -1; hStack--)
	{
		for (int iRod = 0; iRod < field->rods; iRod++)
		{
			char buffer[3] = "__\0";

			if (field->board[iRod][hStack] != 0)
			{
				char* format = field->board[iRod][hStack] > 9 ? "%d\0" : "0%d\0";
				sprintf(buffer, format, field->board[iRod][hStack]);
			}

			printf(buffer);
			printf(" ");
		}
		printf("\n");
	}

	size_t lenDelimeter = field->rods * 3;
	char* delimiter = malloc((lenDelimeter + 1)* sizeof(char));
	for (size_t i = 0; i < lenDelimeter; i++)
		delimiter[i] = i % 2 ? '*' : '-';
	
	delimiter[lenDelimeter] = '\0';
	
	printf("%s\n", delimiter);
	
	for (int i = 0; i < field->rods; i++)
	{
		char* format = i > 9 ? "%d\0" : "0%d\0";
		printf(format, i);
		printf(" ");
	}
}

unsigned char moveBlock(PlayField* field, size_t from, size_t to)
{
	if (field->tops[from] == 0)
	{
		printf("Rod %d has no blocks\n", from);
		return 0;
	}

	size_t fromTop = field->tops[from] - 1;
	if (fromTop < 0)
	{
		printf("The %d rod is empty\n", from);
		return 0;
	}

	size_t block = field->board[from][fromTop];
	size_t targetRodLargestBlock = field->board[to][field->tops[to] - 1];

	if (block > targetRodLargestBlock && targetRodLargestBlock > 0)
	{
		printf("The block %d is larger than the block %d on rod %d\n", block, targetRodLargestBlock, to);
		return 0;
	}

	field->board[from][fromTop] = 0;

	field->board[to][field->tops[to]] = block;

	// int block = field->board[from][field->tops[from]];
	// field->board[to][field->tops[to]] = block;

	field->tops[from]--;
	field->tops[to]++;
	return 1;
}

unsigned char bIsWinningField(PlayField* field)
{
	// Check to see if the first columns are empty
	for (size_t iRod = 0; iRod < field->rods - 1; iRod++)
	{
		for (size_t iStack = 0; iStack < field->stackHeight; iStack++)
		{
			if (field->board[iRod][iStack] != 0)
			return 0;
		}
	}

	return 1;
}
