#include "args.h"
#include <stdio.h>
#include <stdlib.h>

unsigned char parseArgs(int argc, char *argv[], size_t* rods, size_t* stackHeight)
{
	if (argc < 2)
		return 1;

	if (argv[1][0] == '-')
	{
		printf(HOW_TO_USE_MSG "\n", argv[0]);
		return 0;
	}

	*rods = atoi(argv[1]);
	if (argc > 2)
		*stackHeight = atoi(argv[2]);

	return 1;
}
