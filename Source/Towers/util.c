#include "util.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void splitString(const char* src, char*** splitList, size_t* lSize)
{
	size_t len = strlen(src);
	for (size_t i = 1; i < len; i++)
	{
		if (src[i] != ' ')
			continue;
		*lSize += 1;
		i++;
	}

	*lSize += 1;
	*splitList = (char**)malloc(sizeof(char*) * *lSize);
	*splitList[0] = malloc(*lSize * sizeof(char));
	size_t splitStart = 0, splitListIndex = 0;

	for (size_t i = 1; i < len - 1; i++)
	{
		if (src[i] != ' ')
			continue;

		// *splitList[splitListIndex] = (char*) malloc(1);
		strcopyrange(src, &(*splitList[splitListIndex]), splitStart, i);

		i++;
		splitListIndex++;
		splitStart = i;
	}

	// *lSize = 0;
	// *splitList = (char**)malloc(sizeof(char**));
	// char** list = *splitList;

	// for (size_t i = 1; i < len - 1; i++)
	// {
	// 	if (src[i] != ' ')
	// 		continue;

	// 	if (splitStart != 0)
	// 	{
	// 		// Extend the length of the list
	// 		*lSize += 1;
	// 		list = (char**)realloc(list, *lSize);
	// 	}

	// 	list[*lSize] = (char*) malloc(1);
	// 	strcopyrange(src, &(list[*lSize]), splitStart, i);

	// 	i++;
	// 	splitStart = i;
	// }

	// *lSize += 1;
}

char strcopyrange(const char* src, char** dest, int start, int end)
{
	int srcLen = strlen(src);
	if (start >= srcLen || end >= srcLen || end < start)
		return 0;

	// Dereference the double pointer on dest
	char* string = *dest;
	string = (char*)realloc(string, sizeof(char) * (end - start + 1));

	for (int i = start; i < end; i++)
		string[i - start] = src[i];
	string[end - start] = '\0';

	return 1;
}
