#pragma once
#include <stdio.h>
#include <stdlib.h>

#define DEFAULT_STACK_HEIGHT 7
#define DEFAULT_NUM_RODS 3

#define MIN_STACK_HEIGHT 2
#define MIN_NUM_RODS 3

typedef struct
{
    // A multi dimensional array
    int** board;
    // Points to the lowest empty spot on the rod
    int* tops;

    int rods;
    int stackHeight;
} PlayField;


// Creates an empty play field
PlayField newPlayField(size_t rods, size_t stackHeight);

void printPlayField(const PlayField* field);

unsigned char moveBlock(PlayField* field, size_t from, size_t to);

unsigned char bIsWinningField(PlayField* field);
