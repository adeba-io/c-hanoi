#include <stdio.h>
#include <string.h>

#include "board.h"
#include "args.h"

#define TITLE_DECOR "~*~*~*~*~"
#define H_DIALOGUE_BREAK "================================\n"

void printActions();
unsigned char executeMoveAction(const char* action, PlayField* field);

int main(int argc, char *argv[])
{
	printf(TITLE_DECOR "  Towers of Hanoi  " TITLE_DECOR "\n");
	
	size_t rods = 0, stackHeight = 0;
	if (!parseArgs(argc, argv, &rods, &stackHeight))
	{
		return 0;
	}


	// Should parse these from the console args
	
	PlayField field = newPlayField(rods, stackHeight);
	puts("\nGenerated starting board:");
	printPlayField(&field);
	
	printActions();

	putchar('\n');
	putchar('\n');
	puts("Game Start");
	putchar('\n');

	char play = 1;
	size_t totalTurns = 0;

	while (play)
	{
		printf("Turn %d, Input a command: ", totalTurns);
		char action[20];
		gets(action);

		switch (action[0])
		{
			case 'h':
				printActions();
				break;

			case 'q':
				play = 0;
				break;

			case 'p':
				printPlayField(&field);
				puts("");
				break;

			case 'm':
				if (!executeMoveAction(action, &field))
					break;

				totalTurns++;
				if (bIsWinningField(&field))
				{
					play = 0;
					puts("Victory is yours!");
					printf("Completed in %d turns\n", totalTurns);
				}
				break;

			default:
				puts("Command not recognised. Input 'h' to see the help dialogue if necessary");
				break;
		}
	}

	puts("Exiting Game");

	return 1;
}

void printActions()
{
	printf(
		H_DIALOGUE_BREAK
		"h - Print this help dialogue; "
		"q - Quit\n"
		"p - Print the board\n"
		"m <a> <b> - Move Block from <a> to <b>"
		"\n"
		H_DIALOGUE_BREAK
		);
}

unsigned char executeMoveAction(const char* action, PlayField* field)
{
	int from = -1, to = -1;
	for (size_t i = 0; i < strlen(action); i++)
	{
		if (action[i] != ' ')
			continue;
		
		i++;
		char num[2] = "00";

		if (action[i + 1] == ' ') // We've a single digit number
		{
			num[1] = action[i];
		}
		else
		{
			num[0] = action[i];
			num[1] = action[i + 1];
			i++;
		}


		if (from < 0)
		{
			from = atoi(num);
		}
		else
		{
			to = atoi(num);
			break;
		}
	}

	if (to < 0)
	{
		puts("Incorrect format for command");
		return 0;
	}

	return moveBlock(field, from, to);
}
